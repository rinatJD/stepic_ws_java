package main;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import resources.TestResource;
import server.ResourceServerMXBean;
import server.ResourceServerMXBeanImpl;
import servlet.ResoursesServlet;

import javax.management.MBeanServer;
import javax.management.ObjectName;
import java.lang.management.ManagementFactory;

public class Main {
    public static void main(String[] args) throws Exception{

        TestResource testResource = new TestResource();

        ResourceServerMXBean serverMXBean = new ResourceServerMXBeanImpl(testResource);
        MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
        ObjectName name = new ObjectName("Admin:type=ResourceServerController");
        mbs.registerMBean(serverMXBean, name);

        ServletContextHandler handler = new ServletContextHandler(ServletContextHandler.SESSIONS);
        handler.addServlet(new ServletHolder(new ResoursesServlet()), "/resources");

        Server server = new Server(8080);
        server.setHandler(handler);

        server.start();
        System.out.println("Server started");
        server.join();

    }
}
