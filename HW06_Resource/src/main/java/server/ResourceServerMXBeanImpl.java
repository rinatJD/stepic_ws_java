package server;

import resources.TestResource;

public class ResourceServerMXBeanImpl implements ResourceServerMXBean {

    private final TestResource testResource;

    public ResourceServerMXBeanImpl(TestResource testResource) {
        this.testResource = testResource;
    }

    @Override
    public String getname() {
        return testResource.getName();
    }

    @Override
    public int getage() {
        return testResource.getAge();
    }
}
