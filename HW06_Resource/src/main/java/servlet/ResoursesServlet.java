package servlet;

import resources.TestResource;
import sax.ReadXMLFileSAX;
import server.ResourceServerMXBeanImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ResoursesServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String path = req.getParameter("path");
        TestResource resource = (TestResource) ReadXMLFileSAX.readXML(path);
        new ResourceServerMXBeanImpl(resource);

    }
}
