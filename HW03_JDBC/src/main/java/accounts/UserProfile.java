package accounts;

import javax.persistence.*;

@Entity
@Table(name = "user")
public class UserProfile {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    Long id;

    @Column(name = "login", unique = true, updatable = false)
    private final String login;

    @Column(name = "password", unique = true, updatable = false)
    private final String pass;

    public UserProfile(String login, String pass) {
        this.login = login;
        this.pass = pass;
    }

    public String getLogin() {
        return login;
    }

    public String getPass() {
        return pass;
    }

}

