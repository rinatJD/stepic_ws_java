package accounts;


import org.h2.jdbcx.JdbcDataSource;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class AccountService {
    private final Connection connection;

    public AccountService() {
        connection = getH2Connection();
    }

    public void addNewUser(UserProfile userProfile) {

        UsersDAO dao = new UsersDAO(connection);
        try {
            dao.createTable();
            dao.insertUser(userProfile.getLogin(), userProfile.getPass());
            connection.commit();
            return;
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException ex) {
                throw new RuntimeException(ex);
            }
        }

    }

    public UserProfile getUserByLogin(String login) {
        UsersDAO dao = new UsersDAO(connection);
        try {
            Long id = dao.getUserId(login);
            UserProfile profile = dao.get(id);
            return profile;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public static Connection getH2Connection() {
        try {
            String url = "jdbc:h2:./h2db";
            String name = "tully";
            String pass = "tully";

            JdbcDataSource ds = new JdbcDataSource();
            ds.setURL(url);
            ds.setUser(name);
            ds.setPassword(pass);

            Connection connection = DriverManager.getConnection(url, name, pass);
            return connection;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }



}


