package main;

import accountServer.AccountServer;
import accountServer.AccountServerImpl;
import accountServer.AccountServerMXBean;
import accountServer.AccountServerMXBeanImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import servlet.Servlet;

import javax.management.*;
import java.lang.management.ManagementFactory;

public class Main {

    public static void main(String[] args) throws Exception {

        AccountServer accountServer = new AccountServerImpl();

        AccountServerMXBean serverMBean = new AccountServerMXBeanImpl(accountServer);
        MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
        ObjectName name = new ObjectName("Admin:type=AccountServerMBeanImpl");
        mbs.registerMBean(serverMBean, name);

        ServletContextHandler contextHandler = new ServletContextHandler(ServletContextHandler.SESSIONS);
        contextHandler.addServlet(new ServletHolder(new Servlet(accountServer)), "/admin");

        Server server = new Server(8080);
        server.setHandler(contextHandler);

        server.start();
        System.out.println("Server started");
        server.join();

    }
}
