package accountServer;

public class AccountServerMXBeanImpl implements AccountServerMXBean {

    private final AccountServer accountServer;

    public AccountServerMXBeanImpl(AccountServer accountServer) {
        this.accountServer = accountServer;
    }

    @Override
    public int getUsersLimit() {
        return accountServer.getUsersLimit();
    }

    @Override
    public void setUsersLimit(int usersLimit) {
        accountServer.setUsersLimit(usersLimit);
    }
}
