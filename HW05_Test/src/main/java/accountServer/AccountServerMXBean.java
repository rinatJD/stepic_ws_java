package accountServer;

public interface AccountServerMXBean {

    int getUsersLimit();

    void setUsersLimit(int usersLimit);

}
