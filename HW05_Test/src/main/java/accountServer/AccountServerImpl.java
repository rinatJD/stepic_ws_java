package accountServer;

public class AccountServerImpl implements AccountServer {

    int usersLimit = 10;

    @Override
    public int getUsersLimit() {
        return usersLimit;
    }

    @Override
    public void setUsersLimit(int usersLimit) {
        this.usersLimit = usersLimit;
    }
}
